package com.gopi.myvideos.view.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.gopi.myvideos.R;
import com.gopi.myvideos.database.DbHelper;
import com.gopi.myvideos.model.VideoEntity;
import com.gopi.myvideos.view.adapter.RelatedVideoAdapter;

import java.util.ArrayList;

public class VideoDetailActivity extends AppCompatActivity {

    private String TAG = VideoDetailActivity.class.getSimpleName();
    private TextView txtViewVideoTitle;
    private TextView txtViewVideoDesc;
    private ExoPlayer exoPlayer;
    private String videoId;
    private String url;
    private DbHelper dbHelper;
    private ArrayList<VideoEntity> relatedVideoEntities = new ArrayList<>();
    private int playNext = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);

        txtViewVideoTitle = findViewById(R.id.txtViewVideoTitle);
        txtViewVideoDesc = findViewById(R.id.txtViewVideoDesc);
        RecyclerView recyclerViewRelatedVideo = findViewById(R.id.recyclerViewRelatedVideo);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewRelatedVideo.setLayoutManager(mLayoutManager);

        dbHelper = new DbHelper(this);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            videoId = bundle.getString("ID");
            String title = bundle.getString("TITLE");
            String desc = bundle.getString("DESC");
            String thumb = bundle.getString("THUMB");
            url = bundle.getString("URL");

            txtViewVideoTitle.setText(title);
            txtViewVideoDesc.setText(desc);

            relatedVideoEntities = dbHelper.getVideos(videoId);
        }

        RelatedVideoAdapter relatedVideoAdapter = new RelatedVideoAdapter(this, relatedVideoEntities);
        recyclerViewRelatedVideo.setAdapter(relatedVideoAdapter);
        relatedVideoAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //get video pause time nd set exoPlayer
        long videoSeekTime = dbHelper.getVideoPauseTime(videoId);
        Log.v(TAG, "videoSeekTime: " + videoSeekTime);
        initializePlayer(url);
        if (videoSeekTime != 0) {
            exoPlayer.seekTo(videoSeekTime);
        }

        exoPlayer.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady && playbackState == Player.STATE_READY) {
                    Log.v(TAG, "Playing");

                } else if (!playWhenReady && playbackState == Player.STATE_READY) {
                    Log.v(TAG, "Paused");

                    long videoSeekTime = exoPlayer.getCurrentPosition();
                    boolean isUpdated = dbHelper.updatePauseTime(videoId, videoSeekTime);

                    if (isUpdated) {
                        Log.v(TAG, "Video pause time is updated");
                    } else {
                        Log.v(TAG, "Video pause time is not updated");
                    }

                } else if (playWhenReady && playbackState == Player.STATE_ENDED) {
                    Log.v(TAG, "End");

                    if (relatedVideoEntities.size() != playNext) {
                        //play the next video in the queue
                        txtViewVideoTitle.setText(relatedVideoEntities.get(playNext).title);
                        txtViewVideoDesc.setText(relatedVideoEntities.get(playNext).description);
                        initializePlayer(relatedVideoEntities.get(playNext).url);
                        exoPlayer.setPlayWhenReady(true);
                        exoPlayer.addListener(this);
                        playNext++;
                    } else {
                        finish();
                    }
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    private void initializePlayer(String url) {
        // Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        //Initialize the player
        exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);

        //Initialize playerView
        PlayerView playerView = findViewById(R.id.exoPlayer);
        playerView.setPlayer(exoPlayer);

        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, Util.getUserAgent(this, "My Videos"));

        // Produces Extractor instances for parsing the media data.
        //ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        // This is the MediaSource representing the media to be played.
        Uri videoUri = Uri.parse(url);
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(videoUri);

        // Prepare the player with the source.
        exoPlayer.prepare(videoSource);
    }
}
