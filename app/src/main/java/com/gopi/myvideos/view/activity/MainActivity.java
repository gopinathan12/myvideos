package com.gopi.myvideos.view.activity;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gopi.myvideos.R;
import com.gopi.myvideos.database.DbHelper;
import com.gopi.myvideos.model.VideoEntity;
import com.gopi.myvideos.view.adapter.VideoAdapter;
import com.gopi.myvideos.view.interfface.OnVideoActionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnVideoActionListener {

    private String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private VideoAdapter videoAdapter;
    private RequestQueue queue;
    private ArrayList<VideoEntity> videoEntities = new ArrayList<>();
    private ProgressBar progressBar;
    private DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtViewTitle = toolbar.findViewById(R.id.txtViewTitle);
        txtViewTitle.setText(R.string.app_name);

        recyclerView = findViewById(R.id.recyclerViewVideoList);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setMax(100);

        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        dbHelper = new DbHelper(this);
        queue = Volley.newRequestQueue(this);

        videoAdapter = new VideoAdapter(this, videoEntities, this);

        if (checkNetworkConnection()) {
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

        } else {
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        getVideos();

        recyclerView.setAdapter(videoAdapter);
        videoAdapter.notifyDataSetChanged();
    }

    private void getVideos() {
        final String url = "https://interview-e18de.firebaseio.com/media.json?print=pretty";
        videoEntities.clear();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        recyclerView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.optJSONObject(i);
                                String id = object.optString("id");
                                String title = object.optString("title");
                                String description = object.optString("description");
                                String thumb = object.optString("thumb");
                                String url = object.optString("url");

                                VideoEntity videoEntity = new VideoEntity(id, title, description, thumb, url);
                                videoEntities.add(videoEntity);
                                if (!dbHelper.isVideoIdExist(id)) { //inserting into db when id doesn't exit
                                    long isInserted = dbHelper.insertVideo(videoEntity);
                                    if (isInserted > 0) {
                                        Log.v(TAG, "Video is inserted");
                                    } else {
                                        Log.v(TAG, "Video is not inserted");
                                    }
                                }
                            }

                            videoAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.toString());
            }
        });

        queue.add(stringRequest);
    }

    private boolean checkNetworkConnection() {
        ConnectivityManager manager = (ConnectivityManager) this.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (activeNetwork != null) {
            Log.v(TAG, "Network connection is turned ON");
            return true;
        } else {
            Log.v(TAG, "Network connection is turned OFF");
            return false;
        }
    }

    @Override
    public void onVideoChosen(int position) {
        VideoEntity videoEntity = videoEntities.get(position);

        Intent intent = new Intent(this, VideoDetailActivity.class);
        intent.putExtra("ID", videoEntity.id);
        intent.putExtra("TITLE", videoEntity.title);
        intent.putExtra("DESC", videoEntity.description);
        intent.putExtra("THUMB", videoEntity.thumb);
        intent.putExtra("URL", videoEntity.url);
        startActivity(intent);
    }
}
