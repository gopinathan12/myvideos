package com.gopi.myvideos.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gopi.myvideos.R;
import com.gopi.myvideos.model.VideoEntity;

import java.util.ArrayList;

public class RelatedVideoAdapter extends RecyclerView.Adapter<RelatedVideoAdapter.ViewHolder> {

    private ArrayList<VideoEntity> relatedVideoEntities;
    private Context context;

    public RelatedVideoAdapter(Context context, ArrayList<VideoEntity> relatedVideoEntities) {
        this.context = context;
        this.relatedVideoEntities = relatedVideoEntities;
    }

    @NonNull
    @Override
    public RelatedVideoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.related_video_view, viewGroup, false);
        return new RelatedVideoAdapter.ViewHolder(itemView);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgViewVideo;
        TextView txtViewVideoTitle;
        TextView txtViewVideoDesc;
        ConstraintLayout videoView;

        ViewHolder(View view) {
            super(view);
            imgViewVideo = view.findViewById(R.id.imgViewVideo);
            txtViewVideoTitle = view.findViewById(R.id.txtViewVideoTitle);
            txtViewVideoDesc = view.findViewById(R.id.txtViewVideoDesc);
            videoView = view.findViewById(R.id.constraintLayoutRelated);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RelatedVideoAdapter.ViewHolder holder, int position) {
        VideoEntity videoEntity = relatedVideoEntities.get(position);

        holder.txtViewVideoTitle.setText(videoEntity.title);
        holder.txtViewVideoDesc.setText(videoEntity.description);

        if (videoEntity.thumb != null) {
            Glide.with(context).load(videoEntity.thumb).into(holder.imgViewVideo);
        }
    }

    @Override
    public int getItemCount() {
        return relatedVideoEntities.size();
    }
}
