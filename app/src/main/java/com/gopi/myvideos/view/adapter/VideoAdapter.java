package com.gopi.myvideos.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gopi.myvideos.R;
import com.gopi.myvideos.model.VideoEntity;
import com.gopi.myvideos.view.interfface.OnVideoActionListener;

import java.util.ArrayList;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    private ArrayList<VideoEntity> videoEntities;
    private Context context;
    private OnVideoActionListener onVideoActionListener;

    public VideoAdapter(Context context, ArrayList<VideoEntity> videoEntities, OnVideoActionListener onVideoActionListener) {
        this.context = context;
        this.videoEntities = videoEntities;
        this.onVideoActionListener = onVideoActionListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.video_view, viewGroup, false);
        return new ViewHolder(itemView);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgViewVideo;
        TextView txtViewVideoTitle;
        TextView txtViewVideoDesc;
        ConstraintLayout videoView;

        ViewHolder(View view) {
            super(view);
            imgViewVideo = view.findViewById(R.id.imgViewVideo);
            txtViewVideoTitle = view.findViewById(R.id.txtViewVideoTitle);
            txtViewVideoDesc = view.findViewById(R.id.txtViewVideoDesc);
            videoView = view.findViewById(R.id.videoView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        VideoEntity videoEntity = videoEntities.get(position);

        holder.txtViewVideoTitle.setText(videoEntity.title);
        holder.txtViewVideoDesc.setText(videoEntity.description);

        if (videoEntity.thumb != null) {
            Glide.with(context).load(videoEntity.thumb).into(holder.imgViewVideo);
        }

        holder.videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onVideoActionListener.onVideoChosen(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoEntities.size();
    }
}
