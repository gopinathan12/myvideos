package com.gopi.myvideos.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.gopi.myvideos.model.VideoEntity;

import java.util.ArrayList;

public class DbHelper extends SQLiteOpenHelper {

    private static final String TAG = DbHelper.class.getSimpleName();
    private static String DATABASE_NAME = "MyVideosDb";
    private SQLiteDatabase dataBase = null;
    private Context context;

    //table name
    private static final String MY_VIDEOS = "my_videos";

    //fields for MY_VIDEOS
    private static final String VIDEO_ID = "video_id";
    private static final String VIDEO_TITLE = "video_title";
    private static final String VIDEO_DESC = "video_desc";
    private static final String VIDEO_THUMB = "video_thumb";
    private static final String VIDEO_URL = "video_url";
    private static final String PAUSE_TIME = "pause_time";

    // MY_VIDEOS table create statement
    private static final String CREATE_TABLE_MY_VIDEOS = "CREATE TABLE "
            + MY_VIDEOS + "(" + VIDEO_ID + " INTEGER PRIMARY KEY autoincrement," + VIDEO_TITLE + " TEXT, " + VIDEO_DESC + " TEXT, "
            + VIDEO_THUMB + " TEXT," + VIDEO_URL + " TEXT, " + PAUSE_TIME + " INTEGER " + ")";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MY_VIDEOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long insertVideo(VideoEntity videoEntity) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(VIDEO_ID, videoEntity.getId());
        values.put(VIDEO_TITLE, videoEntity.getTitle());
        values.put(VIDEO_DESC, videoEntity.getDescription());
        values.put(VIDEO_THUMB, videoEntity.getThumb());
        values.put(VIDEO_URL, videoEntity.getUrl());

        long videoId = 0;
        try {
            videoId = db.insertOrThrow(MY_VIDEOS, null, values);
            Log.v(TAG, "VIDEO_ID: " + videoId + ", VIDEO_TITLE: " + videoEntity.getTitle() + ", VIDEO_DESC: " + videoEntity.getDescription()
                    + ", VIDEO_THUMB: " + videoEntity.getThumb() + ", VIDEO_URL: " + videoEntity.getUrl());
        } catch (SQLiteConstraintException e) {
            e.printStackTrace();
        }
        return videoId;
    }

    public ArrayList<VideoEntity> getVideos(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c;
        c = db.rawQuery("SELECT * FROM " + MY_VIDEOS, null);
        ArrayList<VideoEntity> videoEntities = new ArrayList<>();
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            do {
                if (!c.getString(0).equals(id)) {
                    VideoEntity videoEntity = new VideoEntity(c.getString(0), c.getString(1), c.getString(2),
                            c.getString(3), c.getString(4));
                    videoEntities.add(videoEntity);
                }
            } while (c.moveToNext());
            c.close();
        }
        return videoEntities;
    }

    public boolean isVideoIdExist(String videoId) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + MY_VIDEOS + " WHERE " + VIDEO_ID + " = '" + videoId + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        boolean isThere = false;
        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                isThere = true;
            }
        }
        c.close();
        db.close();
        return isThere;
    }

    public boolean updatePauseTime(String videoId, long pauseTime) {
        dataBase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(PAUSE_TIME, pauseTime);

        int count = dataBase.update(DbHelper.MY_VIDEOS, values, VIDEO_ID + " = " + videoId, null);
        Log.v(TAG, " VIDEO_ID: " + videoId + ", PAUSE_TIME: " + pauseTime);
        dataBase.close();
        return count > 0;
    }

    public long getVideoPauseTime(String videoId) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT " + PAUSE_TIME + " FROM " + MY_VIDEOS + " WHERE " + VIDEO_ID + " = '" + videoId + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        long pauseTime = 0;
        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                pauseTime = c.getLong(0);
            }
        }
        c.close();
        db.close();
        return pauseTime;
    }
}
