package com.gopi.myvideos.model;

public class VideoEntity {

    public String id;
    public String title;
    public String description;
    public String thumb;
    public String url;

    public VideoEntity(String id, String title, String description, String thumb, String url) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.thumb = thumb;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getThumb() {
        return thumb;
    }

    public String getUrl() {
        return url;
    }
}
